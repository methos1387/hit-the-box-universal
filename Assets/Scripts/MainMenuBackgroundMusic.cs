﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBackgroundMusic : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		if (!UEncryptPrefs.HasKey ("Music Volume")) {
			PlayerPreferences.SetGameEffectsVolume (1);
			PlayerPreferences.SetGameMusicVolume (1);
		}

		gameObject.GetComponent<AudioSource> ().volume = PlayerPreferences.GetGameMusicVolume ();	
	}
}
