﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BonusScript : MonoBehaviour {

	public GameObject explosion;
	public GameObject scorePopUp;
    public GameObject lifePopUp;
	public AudioClip audioClip;
	GameManager gameManager;
	public int addToScore;
	public bool isBomb = true;
	public bool isLife = false;

	private float bonusVolume;

	Rigidbody2D bonusRig;

    void Start() {
        gameManager = FindObjectOfType<GameManager>();
        bonusVolume = gameManager.bonusEffectVolume;
        if (!isBomb && !isLife) {
            transform.localScale = new Vector2(gameManager.bonusSize, gameManager.bonusSize);
        } else if (isBomb) {
            transform.localScale = new Vector2(gameManager.bombSize, gameManager.bombSize);
        } else {
            transform.localScale = new Vector2(1, 1);
        }
    }

    public void BonusTouched () {
        gameManager.UpdateScore(addToScore);

        AudioSource.PlayClipAtPoint (audioClip, new Vector3(0,0,-10), bonusVolume);

		GameObject tempPopUp;

        if (!isLife) {
            tempPopUp = Instantiate(scorePopUp, this.transform.position, Quaternion.identity) as GameObject;
            Destroy(tempPopUp, 2);
        } 
		if (isLife) {
           gameManager.GetComponent<GameManager>().lifeObtained = true;
            if (PlayerPreferences.GetLife() < 2) {
                gameManager.addLife();
                tempPopUp = Instantiate(lifePopUp, this.transform.position, Quaternion.identity) as GameObject;
                Destroy(tempPopUp, 2);
            } else {
                tempPopUp = Instantiate(scorePopUp, this.transform.position, Quaternion.identity) as GameObject;
                Destroy(tempPopUp, 2);
            }
		}
		if (explosion) {
			GameObject tempExplosion;
			tempExplosion = (GameObject) Instantiate (explosion, this.transform.position, Quaternion.identity);
			Destroy (tempExplosion, 1);
		}
        Destroy (gameObject);
	}

	void Update() {
 
			float speedDown = Time.deltaTime * 9f;
			this.transform.Translate (Vector3.down * speedDown);

	}

}
