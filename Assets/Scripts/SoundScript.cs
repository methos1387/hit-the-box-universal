﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundScript : MonoBehaviour
{
    Color32 onColor = new Color32(159,255,248,255);
    Color32 offColor = new Color32(100,120,120,255);

    public GameObject soundFx;
    public GameObject music;

    public AudioSource backgroundMusic;
    public AudioSource buttonSounds;

    bool soundOn;
    bool musicOn;

    Animator soundAnim;

    private void Start() {

        soundAnim = gameObject.GetComponent<Animator>();
        if (!UEncryptPrefs.HasKey("Effects Volume")) {
            PlayerPreferences.SetGameEffectsVolume(1);
        }
        if (!UEncryptPrefs.HasKey("Music Volume")) {
            PlayerPreferences.SetGameMusicVolume(1);
        }
        if (PlayerPreferences.GetGameEffectsVolume() == 1) {
            soundFx.GetComponent<Image>().color = onColor;
            soundOn = true;
            buttonSounds.volume = 1;
        } else {
            soundFx.GetComponent<Image>().color = offColor;
            soundOn = false;
            buttonSounds.volume = 0;

        }
        if (PlayerPreferences.GetGameMusicVolume() == 1) {
            music.GetComponent<Image>().color = onColor;
            musicOn = true;
            backgroundMusic.volume = 1;
        } else {
            music.GetComponent<Image>().color = offColor;
            musicOn = false;
            backgroundMusic.volume = 0;
        }

    }

    public void ToggleSoundFX() {
        if (soundOn) {
            soundOn = false;
            soundFx.GetComponent<Image>().color = offColor;
            PlayerPreferences.SetGameEffectsVolume(0);
            buttonSounds.volume = 0;
        } else {
            soundOn = true;
            soundFx.GetComponent<Image>().color = onColor;
            PlayerPreferences.SetGameEffectsVolume(1);
            buttonSounds.volume = 1;
        }
    }

    public void ToggleMusic() {
        if (musicOn) {
            musicOn = false;
            music.GetComponent<Image>().color = offColor;
            PlayerPreferences.SetGameMusicVolume(0);
            backgroundMusic.volume = 0;
        } else {
            musicOn = true;
            music.GetComponent<Image>().color = onColor;
            PlayerPreferences.SetGameMusicVolume(1);
            backgroundMusic.volume = 1;
        }
    }

    public void OpenSoundSettings() {
        soundAnim.SetBool("Open", true);
    }

    public void CloseSoundSettings() {
        soundAnim.SetBool("Open", false);
    }
}
