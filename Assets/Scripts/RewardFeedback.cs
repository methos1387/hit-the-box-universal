﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardFeedback : MonoBehaviour {

	public void AddFeedback(string value) {
		this.GetComponent<Text> ().text = value;
		this.GetComponent<Animator>().SetTrigger("ShowReward");
	}

}
