﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoxScriptC : MonoBehaviour {
    public GameObject[] boxExplosions = new GameObject[4];
    public GameObject[] iceBoxExpl = new GameObject[4];
    public GameObject[] harmonyExpl = new GameObject[4];
    public GameObject[] holyExpl = new GameObject[4];
	public GameManager gameManager;
	public GameObject pauseBtn;
	public GameObject speedGui;

	private float addSpeed = 0.5f;
	private Rigidbody2D boxRig;

    private string boxName;
    AudioSource audio;

    GameObject[] UsingExplosion;

    private int particleSize;

	void Start() {
        if (!UEncryptPrefs.HasKey("boxSize")) {
            PlayerPreferences.SetBoxSize(1);
        }
        //Get the prite name to load
        boxName = "box" + PlayerPreferences.GetBoxUsing().ToString()+(PlayerPreferences.GetBoxSize()-1).ToString();
        //Load the sprite from resources
        var sp = Resources.Load<Sprite>(boxName);
        //Attach sprite to renderer
        transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = sp;
		boxRig = GetComponent<Rigidbody2D> ();
		gameObject.GetComponent<AudioSource> ().volume = PlayerPreferences.GetGameEffectsVolume ();
		transform.position = new Vector3 (0, 8f, 1);
        audio = gameObject.GetComponent<AudioSource>();
        particleSize = PlayerPreferences.GetBoxSize() - 1;
        GetBoxSkin();
    }

    public void GetBoxSkin() {
        int boxUsed = PlayerPreferences.GetBoxUsing();
        switch(boxUsed) {
            case 0:
                UsingExplosion = boxExplosions;
                break;
            case 1:
                UsingExplosion = iceBoxExpl;
                return;
            case 2:
                UsingExplosion = harmonyExpl;
                return;
            case 3:
                UsingExplosion = holyExpl;
                return;
            default:
                UsingExplosion = iceBoxExpl;
                return;
        }
    }

	public void BoxTouched () {
        gameManager.UpdateScore(100);

		if (!pauseBtn.activeSelf) {
			pauseBtn.SetActive (true);
		}
		if (gameManager.boxStopped) {
			gameManager.speed = gameManager.pauseSpeed;
			gameManager.boxStopped = false;
		}
        gameManager.SpawnLife();
        gameManager.spawnBonusObject ();
		gameManager.canSpawnBonus = true;

		if (gameManager.speed <= 50) {
			gameManager.speed += addSpeed;
			speedGui.GetComponent<Text> ().text = gameManager.speed.ToString ("0.00");
		} else {
			speedGui.GetComponent<Text> ().text = "MAX";
		}
        Instantiate(UsingExplosion[particleSize], this.transform.position, Quaternion.identity);
        audio.Play ();
		float randomPositionOfTheBox = Random.Range (GameManager.l, GameManager.r);
		this.transform.position = new Vector3 (randomPositionOfTheBox, 9.0f, transform.position.z); 
	}

	void Update() {
        float speedDown = Time.deltaTime * gameManager.speed;
        this.transform.Translate(Vector3.down * speedDown);
    }

}

