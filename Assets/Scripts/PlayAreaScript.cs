﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAreaScript : MonoBehaviour {

	public GameObject leftWall;
	public GameObject rightWall;

	// Use this for initialization
	void Start() {
		
		leftWall.transform.position = new Vector2(GameManager.l - 2.5f, 0);
		rightWall.transform.position = new Vector2(GameManager.r + 2.5f, 0);

	}
}
