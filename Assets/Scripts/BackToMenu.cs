﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BackToMenu : MonoBehaviour {


    private AudioSource myAudio;



	void Start() {
		myAudio = gameObject.GetComponent<AudioSource> ();
		myAudio.volume = PlayerPreferences.GetGameEffectsVolume ();
	}

	public void BackToMainMenu() {
		StartCoroutine (waifForAudioMenu ());
	}


	IEnumerator waifForAudioMenu() {
		myAudio.Play ();
		yield return new WaitForSeconds (myAudio.clip.length);
		SceneManager.LoadScene("htb_scene0", LoadSceneMode.Single);

	}
}
