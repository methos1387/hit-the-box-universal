﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public Button pauseButton;
	public Button continueButton;
	public GameObject theBox;
	public GameObject[] Bonus;
	public GameObject[] Life;
	public GameObject spawnLife;
	public GameObject bonusStarter;
	public GameObject continueBtn;
    public GameObject useLife;
	public GameObject pauseBtn;
	//public GameObject reward;
	public GameObject rateGame;
	public GameObject buyUpgrade;
    //public GameObject continueWithAd;
    public Animator canvasAnimator;
    //public AdManager adManager;
    public Text scoreText;

    public GameObject lifeUI;
    public GameObject inGameMenu;

	public float speed = 0f;
	public float pauseSpeed = 0f;
	public bool boxStopped = false;
    //public bool continuedWithAd = false;

    public AudioSource music;

	private float InstantiationTimer;
    private bool CanSpawnBonusRain = true;
    private bool bonusRain = true;

    public bool lifeObtained = false;
    private bool musicOn = false;
	private AudioSource buttonSound;
	private Camera cam;
    private int totalGold;

	public static int life;
	public static float l, r;
	public static int yourScore = 0;
	public bool canSpawnBonus = false;

    //Bonus data for bonusScript
    public int bonusEffectVolume;
    public float bonusSize;
    public float bombSize;

    void Awake() {
        if (!UEncryptPrefs.HasKey("UsingBox")) {
            PlayerPreferences.SetBoxUsing(0);
        }
        life = PlayerPreferences.GetLife();
        if (life < 1) {
            life = 1;
            PlayerPreferences.SetLife(1);
        }
		yourScore = 0;
		buttonSound = gameObject.GetComponent<AudioSource> ();
		buttonSound.volume = PlayerPreferences.GetGameEffectsVolume ();

		//Clamp SpawnBonus to camera
		cam = Camera.FindObjectOfType<Camera> ();
		Vector3 leftMargin = cam.ViewportToWorldPoint(new Vector3(0, 1, cam.farClipPlane));
		Vector3 rightMargin = cam.ViewportToWorldPoint(new Vector3(1, 1, cam.farClipPlane));
		l = leftMargin.x + 1f;
		r = rightMargin.x - 1f;

        GetBonusData();
        totalGold = PlayerPreferences.GetGold();
    }

	public void pauseGame() {
        PlayerPreferences.SetGold(totalGold);
		buttonSound.Play ();
		pauseSpeed = speed;
		speed = 0f;
		canSpawnBonus = false;
		theBox.SetActive (false);
	}

	public void unpause() { 
        buttonSound.Play ();
		speed = pauseSpeed;
		theBox.SetActive (true);
	}

    public void useLifeContinue() {
        life -= 1;
        PlayerPreferences.SetLife(PlayerPreferences.GetLife() - 1);
        speed /= 2;
    }

    public void UpdateScore(int score) {
        yourScore += score;
        totalGold += score / 100;
        scoreText.text = "Score : " + yourScore;
    }

    //public void rewardedContinue() {
    //    continuedWithAd = true;
    //    inGameMenu.SetActive(false);
    //    speed = pauseSpeed / 2;
    //    theBox.SetActive(true);
    //}

	public void addLife() {
		life += 1;
		PlayerPreferences.SetLife (PlayerPreferences.GetLife () + 1);
	}

    public void loadScene(string scene)
    {
        //if (!UEncryptPrefs.HasKey("AdChance"))
        //{
        //    UEncryptPrefs.SetInt("AdChance", 1);
        //}
        //if (UEncryptPrefs.GetInt("AdChance") > 100)
        //{
        //    adManager.ShowMyAd(3);
        //}
        //else
        //{
        //    GenerateAdValue();
        //    yourScore = 0;
            StartCoroutine(playAudioAndLoadScene(scene));
        //}
    }

    //Waiting for audio to play before scene load
    IEnumerator playAudioAndLoadScene(string scene){
		buttonSound.Play ();
        canvasAnimator.SetTrigger("Load");
		yield return new WaitForSeconds (0.5f);
		SceneManager.LoadSceneAsync (scene, LoadSceneMode.Single);
	}

    //public void GenerateAdValue() {
    //    int addValue = Random.Range(15, 20);
    //    if (UEncryptPrefs.HasKey("AdChance")) {
    //        UEncryptPrefs.SetInt("AdChance", UEncryptPrefs.GetInt("AdChance") + addValue);
    //    } else {
    //        UEncryptPrefs.SetInt("AdChance", addValue);
    //    }
        
    //}

	#if UNITY_ANDROID
	#region postScore
	void postScore() {
		if (Social.localUser.authenticated) {
				Social.ReportScore (yourScore, "CgkI1-KJrKkQEAIQCQ", (bool success) => {
				});
	
		}
	}
	#endregion


	#region Achievement
	void checkAchievement() {
		if (Social.localUser.authenticated) {
			if (yourScore <= -1000) {
				Social.ReportProgress("CgkI1-KJrKkQEAIQBw", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 10000) {
				Social.ReportProgress("CgkI1-KJrKkQEAIQAw", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 15000) {
				Social.ReportProgress("CgkI1-KJrKkQEAIQBA", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 20000) {
				Social.ReportProgress("CgkI1-KJrKkQEAIQBQ", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 25000) {
				Social.ReportProgress("CgkI1-KJrKkQEAIQBg", 100.0f, (bool success) => {
				});
			}

		}
	}
	#endregion
	#endif


	#if UNITY_IOS  && !UNITY_EDITOR
	#region postScore
	void postScore() {
		if (Social.localUser.authenticated) {
			
				Social.ReportScore (yourScore, "hittheboxnormal", (bool success) => {
				});
		
		}
	}
	#endregion


	#region Achievement
	void checkAchievement() {
		if (Social.localUser.authenticated) {
			if (yourScore <= -1000) {
				Social.ReportProgress("lessIsMore", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 10000) {
				Social.ReportProgress("goodStart", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 15000) {
				Social.ReportProgress("notBad", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 20000) {
				Social.ReportProgress("master", 100.0f, (bool success) => {
				});
			}
			if (yourScore >= 25000) {
				Social.ReportProgress("impossible", 100.0f, (bool success) => {
				});
			}

		}
	}
	#endregion
	#endif

	public void gameOver() {
        PlayerPreferences.SetGold(totalGold);
        StartCoroutine(cam.GetComponent<CameraShaker>().Shake(0.14f, 0.4f));
        if (life >= 1) {
            useLifeContinue();
        //} else if (yourScore > 5000 && !continuedWithAd) {
        //    pauseGame();
        //    pauseBtn.SetActive(false);
        //    lifeUI.SetActive(false);
        //    continueWithAd.SetActive(true);
        } else {
            pauseButton.onClick.Invoke();
            continueBtn.SetActive(false);
            //BuyUpgrade();
            //rateTheGame();
            //reward.SetActive(true);
        }

        //Put the box back to starting point.
		theBox.transform.position = new Vector3 (transform.position.x, 8.0f, transform.position.z);

		//Post score to leaderboard each time gameover was called
#if UNITY_IOS && !UNITY_EDITOR
		postScore ();
		checkAchievement ();
#endif
	}

	//public void BuyUpgrade() {
	//	if (PlayerPreferences.GetGold () >= 6000) {
	//		float chance = Random.value;
	//		if (chance > 0.9) {
	//			Instantiate (buyUpgrade, new Vector3 (0, 0, 0), Quaternion.identity,  reward.GetComponent<Transform>());
	//		} else {
	//			return;
	//		}
	//	} else {
	//		return;
	//	}
	//}

	//public void rateTheGame() {
	//	if (PlayerPreferences.GetRate () != 0) {
	//		float chance = Random.value;
	//		if (chance > 0.9) {
 //               Instantiate (rateGame, new Vector3 (0, 0, 0), Quaternion.identity,  reward.GetComponent<Transform>());
	//		} else {
	//			return;
	//		}
	//	} else {
	//		return;
	//	}
	//}

#region spawn bonus
	public void spawnBonusObject() {
		if (canSpawnBonus) {
			float spawnPossibility = Random.Range (0f, 3f);
			if (spawnPossibility >= 1f) {
                Instantiate(Bonus[Random.Range(0, Bonus.Length)], new Vector3(Random.Range(l, r), 9f, 5f), Quaternion.identity);
            }
		} else {
			return;
		}
	}
#endregion

	public void SpawnLife() {
        if (!lifeObtained) {
            float spawnPossibility = Random.Range(0f, 100f);
            if (spawnPossibility >= 99f) { 
                Instantiate(spawnLife, new Vector3(Random.Range(l, r), 9f, 5f), Quaternion.identity);
            }
            if (spawnPossibility <= 0.7f && CanSpawnBonusRain) {
                CanSpawnBonusRain = false;
                Instantiate(bonusStarter, new Vector3(Random.Range(l, r), 9f, 5f), Quaternion.identity);
                StartCoroutine(bonusStarterCountdown());
            }
        }
	}

    IEnumerator bonusStarterCountdown() {
        yield return new WaitForSeconds(15);
        CanSpawnBonusRain = true;
    }

#region bonus rain
	IEnumerator bonusRainStart() {
		bonusRain = false;
		for (int i = 0; i < 30; i++) {
            Instantiate(Bonus[Random.Range(0, Bonus.Length)], new Vector3(Random.Range(l, r), 9f, transform.position.z), Quaternion.identity);
            yield return new WaitForSeconds (0.3f);
			}
		}
#endregion

	public void StartBonusRain() {
       
		StartCoroutine (bonusRainStart ());
	}

    public void GetBonusData() {
        if (PlayerPreferences.GetGameEffectsVolume() != 1) {
            bonusEffectVolume = 0;
        } else {
            bonusEffectVolume = 1;
        }
        if (UEncryptPrefs.HasKey("bonusSize")) {
            bonusSize = PlayerPreferences.GetBonusSize();
        } else {
            bonusSize = 1;
        }
        if (UEncryptPrefs.HasKey("bombSize")) {
            bombSize = PlayerPreferences.GetBombSize();
        } else {
            bombSize = 1;
        }

    }


	void Update() {
        if (yourScore > 2000 && !musicOn) {
            musicOn = true;
            music.Play();
        }
		if (yourScore > 4000 && bonusRain) {
			StartCoroutine(bonusRainStart ());
            
        }
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space)) {
            ScreenCapture.CaptureScreenshot("ScreenShot");
        }
#endif
    }



}