﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsManager : MonoBehaviour {

	public static int currentGold;
    public Animator anim;
	public Button boxUpgrade;
	public Button bonusUpgrade;
	public Button bombUpgrade;

    public Image defaultBox;
    public Image iceBox;
    public Image harmonyBox;
    public Image holyBox;


    //public Button gem;
    public GameObject currentGoldText;

	int boxUpgradePrice;
	int bonusUpgradePrice;
	int bombUpgradePrice;

    int iceBoxPrice = 15000;
    int harmonyBoxPrice = 15000;
    int holyBoxPrice = 25000;

    bool canBuyBox = true;
	bool canBuyBonus = true;
	bool canBuyBomb = true;
	//bool canBuyGem = true;

	Text boxPriceText;
	Text bonusPriceText;
	Text bombPriceText;
	//Text gemText;

	string buy;
	string bought;
	string max;
    string select;
    string set;

	void Start(){
        DetectBoxUsing();

        buy = "";
		bought = Lang.GetLang ("Bought");
		max = Lang.GetLang ("Max");
        select = Lang.GetLang("Select");
        set = Lang.GetLang("Using");

		if (!UEncryptPrefs.HasKey("boxSize")) {
			PlayerPreferences.SetBoxSize (1);
		}

		if (!UEncryptPrefs.HasKey ("bonusSize")) {
			PlayerPreferences.SetBonusSize (1.0f);
		}

		if (!UEncryptPrefs.HasKey ("bombSize")) {
			PlayerPreferences.SetBombSize (1.0f);
		}

        //if (!UEncryptPrefs.HasKey ("gem")) {
        //	PlayerPreferences.SetGemActive (0);
        //}
        currentGold = PlayerPreferences.GetGold ();
		currentGoldText.GetComponent<Text> ().text = currentGold.ToString ();
		boxPriceText = boxUpgrade.GetComponentInChildren<Text>();
		bonusPriceText = bonusUpgrade.GetComponentInChildren<Text> ();
		bombPriceText = bombUpgrade.GetComponentInChildren<Text> ();
		//gemText = gem.GetComponentInChildren<Text> ();

		CheckBoxPrice ();
		CheckBonusPrice ();
		CheckBombPrice ();
        DetectBoxUsing();
		//isGemActive ();
		boxBuyColor ();
		bonusBuyColor ();
		bombBuyColor ();
        iceBoxBuyColor();
        HarmonyBoxBuyColor();
        HolyBoxBuyColor();

	}

	//public void buyGem() {
	//	playAudio ();
	//	if ((currentGold >= 8000) && (canBuyGem)) {
	//		currentGold -= 8000;
	//		PlayerPreferences.SetGold (currentGold);
	//		currentGoldText.GetComponent<Text> ().text = currentGold.ToString ();
	//		PlayerPreferences.SetGemActive (1);
	//		isGemActive ();
	//		boxBuyColor ();
	//		bonusBuyColor ();
	//		bombBuyColor ();
	//	}
	//}

	public void buyBoxUpgrade () {
		playAudio ();
		if ((currentGold >= boxUpgradePrice) && (canBuyBox)) {
			currentGold -= boxUpgradePrice;
			PlayerPreferences.SetGold (currentGold);
			currentGoldText.GetComponent<Text> ().text = currentGold.ToString ();
			PlayerPreferences.SetBoxSize (PlayerPreferences.GetBoxSize () + 1);
			CheckBoxPrice ();
            ColorChecker();
            //isGemActive ();
        }
	}

	public void buyBonusUpgrade () {
		playAudio ();
		if ((currentGold >= bonusUpgradePrice) && (canBuyBonus)) {
			currentGold -= bonusUpgradePrice;
			PlayerPreferences.SetGold (currentGold);
			currentGoldText.GetComponent<Text> ().text = currentGold.ToString ();
			PlayerPreferences.SetBonusSize (PlayerPreferences.GetBonusSize () + 0.1f);
			CheckBonusPrice ();
            ColorChecker();
            //isGemActive ();
        } 
	}

	public void buyBombUpgrade() {
		playAudio ();
		if ((currentGold >= bombUpgradePrice) && (canBuyBomb)) {
			currentGold -= bombUpgradePrice;
			PlayerPreferences.SetGold (currentGold);
			currentGoldText.GetComponent<Text> ().text = currentGold.ToString ();
			PlayerPreferences.SetBombSize (PlayerPreferences.GetBombSize () - 0.1f);
			CheckBombPrice ();
            ColorChecker();
            //isGemActive ();
        }
	}

    public void SetDefaultBox() {
        playAudio();
        PlayerPreferences.SetBoxUsing(0);
        DetectBoxUsing();
    }

    public void BuyIceBox() {
        playAudio();
        if ((currentGold >= iceBoxPrice && PlayerPreferences.IceBoxBought() != 1)) {
            currentGold -= iceBoxPrice;
            PlayerPreferences.SetGold(currentGold);
            currentGoldText.GetComponent<Text>().text = currentGold.ToString();
            PlayerPreferences.BuyIceBox();
            PlayerPreferences.SetBoxUsing(1);
            ColorChecker();
            //isGemActive ();
            DetectBoxUsing();
        } else if (PlayerPreferences.IceBoxBought() == 1) {
            PlayerPreferences.SetBoxUsing(1);
            DetectBoxUsing();
        }
    }

    public void BuyHarmonyBox() {
        playAudio();
        if ((currentGold >= harmonyBoxPrice && PlayerPreferences.HarmonyBoxBought() != 1)) {
            currentGold -= harmonyBoxPrice;
            PlayerPreferences.SetGold(currentGold);
            currentGoldText.GetComponent<Text>().text = currentGold.ToString();
            PlayerPreferences.BuyHarmonyBox();
            PlayerPreferences.SetBoxUsing(2);
            ColorChecker();
            //isGemActive ();
            DetectBoxUsing();
        } else if (PlayerPreferences.IceBoxBought() == 1) {
            PlayerPreferences.SetBoxUsing(2);
            DetectBoxUsing();
        }
    }

    public void BuyHolyBox() {
        playAudio();
        if ((currentGold >= holyBoxPrice && PlayerPreferences.HolyBoxBought() != 1)) {
            currentGold -= holyBoxPrice;
            PlayerPreferences.SetGold(currentGold);
            currentGoldText.GetComponent<Text>().text = currentGold.ToString();
            PlayerPreferences.BuyHolyBox();
            PlayerPreferences.SetBoxUsing(3);
            ColorChecker();
            //isGemActive ();
            DetectBoxUsing();
        } else if (PlayerPreferences.HolyBoxBought() == 1) {
            PlayerPreferences.SetBoxUsing(3);
            DetectBoxUsing();
        }
    }

    public void DetectBoxUsing() {
        switch (PlayerPreferences.GetBoxUsing()) {
            case 0:
                defaultBox.transform.GetChild(0).GetComponent<Text>().text = set;
                if (PlayerPreferences.IceBoxBought() == 1) {
                    iceBox.transform.GetChild(0).GetComponent<Text>().text = select;
                } 
                if (PlayerPreferences.HarmonyBoxBought() == 1) {
                    harmonyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                if (PlayerPreferences.HolyBoxBought() == 1) {
                    holyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                return;
            case 1:
                defaultBox.transform.GetChild(0).GetComponent<Text>().text = select;
                if (PlayerPreferences.IceBoxBought() == 1) {
                    iceBox.transform.GetChild(0).GetComponent<Text>().text = set;
                }
                if (PlayerPreferences.HarmonyBoxBought() == 1) {
                    harmonyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                if (PlayerPreferences.HolyBoxBought() == 1) {
                    holyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                return;
            case 2:
                defaultBox.transform.GetChild(0).GetComponent<Text>().text = select;
                if (PlayerPreferences.IceBoxBought() == 1) {
                    iceBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                if (PlayerPreferences.HarmonyBoxBought() == 1) {
                    harmonyBox.transform.GetChild(0).GetComponent<Text>().text = set;
                }
                if (PlayerPreferences.HolyBoxBought() == 1) {
                    holyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                return;
            case 3:
                defaultBox.transform.GetChild(0).GetComponent<Text>().text = select;
                if (PlayerPreferences.IceBoxBought() == 1) {
                    iceBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                if (PlayerPreferences.HarmonyBoxBought() == 1) {
                    harmonyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                if (PlayerPreferences.HolyBoxBought() == 1) {
                    holyBox.transform.GetChild(0).GetComponent<Text>().text = set;
                }
                return;
            default:
                defaultBox.transform.GetChild(0).GetComponent<Text>().text = set;
                if (PlayerPreferences.IceBoxBought() == 1) {
                    iceBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                if (PlayerPreferences.HarmonyBoxBought() == 1) {
                    harmonyBox.transform.GetChild(0).GetComponent<Text>().text = select;
                }
                return;

        }
    }

	void CheckBoxPrice() {
		float boxSize = PlayerPreferences.GetBoxSize ();
		if (boxSize == 1) {
			boxUpgradePrice = 800;
            //boxPriceText.text = buy + " : " + boxUpgradePrice + " Gold";
            boxPriceText.text = boxUpgradePrice.ToString();
        } else if (boxSize == 2) {
			boxUpgradePrice = 1500;
            //boxPriceText.text = buy + " : " + boxUpgradePrice + " Gold";
            boxPriceText.text = boxUpgradePrice.ToString();
        } else if ((boxSize == 3)) {
			boxUpgradePrice = 3000;
            //boxPriceText.text = buy + " : " + boxUpgradePrice + " Gold";
            boxPriceText.text = boxUpgradePrice.ToString();
        } else {
			canBuyBox = false;
			boxPriceText.text = max;
		}
	}

	void CheckBonusPrice() {
		float bonusSize = PlayerPreferences.GetBonusSize ();
		if ((bonusSize >= 0.9f) && (bonusSize <= 1.01f)) {
			bonusUpgradePrice = 800;
            //bonusPriceText.text = buy + " : " + bonusUpgradePrice + " Gold";
            bonusPriceText.text = bonusUpgradePrice.ToString();
        } else if ((bonusSize >= 1.09f) && (bonusSize <= 1.11f)) {
			bonusUpgradePrice = 1500;
            //bonusPriceText.text = buy + " : " + bonusUpgradePrice + " Gold";
            bonusPriceText.text = bonusUpgradePrice.ToString();
        } else if ((bonusSize >= 1.19f) && (bonusSize <= 1.21f)) {
			bonusUpgradePrice = 3000;
            //bonusPriceText.text = buy + " : " + bonusUpgradePrice + " Gold";
            bonusPriceText.text = bonusUpgradePrice.ToString();
        } else if ((bonusSize >= 1.29f) && (bonusSize <= 1.31f)) {
			bonusUpgradePrice = 6000;
            //bonusPriceText.text = buy + " : " + bonusUpgradePrice + " Gold";
            bonusPriceText.text = bonusUpgradePrice.ToString();
        } else if ((bonusSize >= 1.39f) && (bonusSize <= 1.41f)) {
			bonusUpgradePrice = 10000;
            //bonusPriceText.text = buy + " : " + bonusUpgradePrice + " Gold";
            bonusPriceText.text = bonusUpgradePrice.ToString();
        } else {
			canBuyBonus = false;
			bonusPriceText.text = max;
		}
	}

	void CheckBombPrice() {
		float bombSize = PlayerPreferences.GetBombSize ();
		if ((bombSize >= 0.99f) && (bombSize <= 1.01f)) {
			bombUpgradePrice = 800;
            //bombPriceText.text = buy + " : " + bombUpgradePrice + " Gold";
            bombPriceText.text = bombUpgradePrice.ToString();
        } else if ((bombSize >= 0.89f) && (bombSize <= 0.91f)) {
			bombUpgradePrice = 1500;
            //bombPriceText.text = buy + " : " + bombUpgradePrice + " Gold";
            bombPriceText.text = bombUpgradePrice.ToString();
        } else if ((bombSize >= 0.79f) && (bombSize <= 0.81f)) {
			bombUpgradePrice = 3000;
            //bombPriceText.text = buy + " : " + bombUpgradePrice + " Gold";
            bombPriceText.text = bombUpgradePrice.ToString();
        } else if ((bombSize >= 0.69f) && (bombSize <= 0.71f)) {
			bombUpgradePrice = 6000;
            //bombPriceText.text = buy + " : " + bombUpgradePrice + " Gold";
            bombPriceText.text = bombUpgradePrice.ToString();
        } else if ((bombSize >= 0.59f) && (bombSize <= 0.61f)) {
			bombUpgradePrice = 10000;
            //bombPriceText.text = buy + " : " + bombUpgradePrice + " Gold";
            bombPriceText.text = bombUpgradePrice.ToString();
        } else {
			canBuyBomb = false;
			bombPriceText.text = max;
		}
	}

    public void ColorChecker() {
        CheckBombPrice();
        bonusBuyColor();
        bombBuyColor();
        boxBuyColor();
        iceBoxBuyColor();
        HarmonyBoxBuyColor();
        HolyBoxBuyColor();
    }

	void boxBuyColor() {
		if (boxPriceText.text != max) {
			if (currentGold >= boxUpgradePrice) {
				boxUpgrade.GetComponent<Image> ().color = new Color32 (130, 190, 120, 255);
			} else {
				boxUpgrade.GetComponent<Image> ().color = new Color32 (200, 80, 80, 255);
			}
		} else {
			boxUpgrade.GetComponent<Image> ().color = new Color32 (175, 175, 175, 255);
		}
	}

	void bonusBuyColor() {
		if (bonusPriceText.text != max) {
			if (currentGold >= bonusUpgradePrice) {
				bonusUpgrade.GetComponent<Image> ().color = new Color32 (130, 190, 120, 255);
			} else {
				bonusUpgrade.GetComponent<Image> ().color = new Color32 (200, 80, 80, 255);
			}
		} else {
			bonusUpgrade.GetComponent<Image> ().color = new Color32 (175, 175, 175, 255);
		}
	}

	void bombBuyColor() {
		if (bombPriceText.text != max) {
			if (currentGold >= bombUpgradePrice) {
				bombUpgrade.GetComponent<Image> ().color = new Color32 (130, 190, 120, 255);
			} else {
				bombUpgrade.GetComponent<Image> ().color = new Color32 (200, 80, 80, 255);
			}
		} else {
			bombUpgrade.GetComponent<Image> ().color = new Color32 (175, 175, 175, 255);
		}
	}

    void iceBoxBuyColor() {
        if (currentGold >= iceBoxPrice || PlayerPreferences.IceBoxBought() == 1) {
            iceBox.color = new Color32(130, 190, 120, 255);
        } else {
            iceBox.color = new Color32(200, 80, 80, 255);
        }
    }

    void HarmonyBoxBuyColor() {
        if (currentGold >= harmonyBoxPrice || PlayerPreferences.HarmonyBoxBought() == 1) {
            harmonyBox.color = new Color32(130, 190, 120, 255);
        } else {
            harmonyBox.color = new Color32(200, 80, 80, 255);
        }
    }

    void HolyBoxBuyColor() {
        if (currentGold >= holyBoxPrice || PlayerPreferences.HolyBoxBought() == 1) {
            holyBox.color = new Color32(130, 190, 120, 255);
        } else {
            holyBox.color = new Color32(200, 80, 80, 255);
        }
    }

    //void isGemActive() {
    //	if (PlayerPreferences.GetGem () == 1) {
    //		canBuyGem = false;
    //		gemText.text = bought;
    //		gem.GetComponent<Image> ().color = new Color32 (175, 175, 175, 255);
    //	} else if ((PlayerPreferences.GetGem () == 0) && (currentGold >= 3000)) {
    //		gem.GetComponent<Image> ().color = new Color32 (130, 190, 120, 255);
    //	} else {
    //		gem.GetComponent<Image> ().color = new Color32 (200, 80, 80, 255);
    //	}
    //}

    public void playAudio() {
		gameObject.GetComponent<AudioSource> ().Play ();
	}

    public void BackToMenu() {
        StartCoroutine(WaitForMenu());
    }

    IEnumerator WaitForMenu() {
        playAudio();
        anim.SetTrigger("Load");
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadSceneAsync("htb_scene0", LoadSceneMode.Single);
    }


}
