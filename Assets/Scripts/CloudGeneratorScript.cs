﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGeneratorScript : MonoBehaviour
{

    public GameObject[] spawnPoints = new GameObject[4];
    public GameObject[] clouds = new GameObject[10];

    int randomCloud;
    int randomSpawnPoint;
    int maxClouds = 0;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("RepeatCloudSpawn", 8, 10f);
    }


    void RepeatCloudSpawn() {
        SpawnCloud();
        maxClouds += 1;
        if (maxClouds >= 5) {
            StartCoroutine(CloudDelay());
        }
    }

    IEnumerator CloudDelay() {
        yield return new WaitForSeconds(20f);
    }

    public void SpawnCloud()
    {
        randomCloud = Random.Range(0, clouds.Length);
        randomSpawnPoint = Random.Range(0, spawnPoints.Length);
        Instantiate(clouds[randomCloud], spawnPoints[randomSpawnPoint].transform.position, Quaternion.identity);

    }

}
