﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusStarterScript : MonoBehaviour {

	GameManager gameManager;
    public GameObject particleObject;


	void Start() {
		gameObject.GetComponent<AudioSource> ().volume = PlayerPreferences.GetGameEffectsVolume ();
		gameManager = GameObject.FindObjectOfType<GameManager> ();
	}

	public void BonusStarter() {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
        Instantiate(particleObject, this.transform.position, Quaternion.identity);
		StartCoroutine(waitForAudio());

	}

	IEnumerator waitForAudio() {
		gameObject.GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds (gameObject.GetComponent<AudioSource>().clip.length);
		gameManager.StartBonusRain ();
		Destroy (gameObject);
	}

	// Update is called once per frame
	void Update () {
        float speedDown = Time.deltaTime * 9f;
        this.transform.Translate(Vector3.down * speedDown);
    }
}
