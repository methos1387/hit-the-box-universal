﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdContinueScript : MonoBehaviour
{
    public GameObject inGameMenu;
    public GameObject parent;
    public GameObject doubleGold;
    Image timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        timer.fillAmount -= 0.2f * Time.deltaTime;

        if (timer.fillAmount == 0) {
            inGameMenu.SetActive(true);
            parent.SetActive(false);
            doubleGold.SetActive(true);
        }

    }
}
