﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background : MonoBehaviour {

	public GameObject lightning;
	public GameObject rain;
	public GameObject scoreText;

	private float scoreChecker;

	Color night = new Color32(69, 69, 69, 255);
	Color white = new Color32(200, 200, 200, 200);

	void Start() {
		InvokeRepeating ("StartLightning", 2f, Random.Range(2f, 6f));
	}

	public void StartLightning() {
		StartCoroutine (WaitForLightning ());
	}

	IEnumerator WaitForLightning() {
		if (scoreChecker >= 9000f) {
			rain.SetActive (true);
			scoreText.GetComponent<Text> ().color = white;
			gameObject.transform.GetChild (0).GetComponent<SpriteRenderer> ().color = night;
			lightning.transform.position = new Vector3 (Random.Range (-7f, 7f), 0, 0);
			lightning.transform.localEulerAngles = new Vector3 (0, 0, Random.Range (-20f, 20f));
			lightning.SetActive (true);
			yield return new WaitForSeconds (1);
			lightning.SetActive (false);
		}
	}

	void Update() {
		scoreChecker = GameManager.yourScore;
	}

}
