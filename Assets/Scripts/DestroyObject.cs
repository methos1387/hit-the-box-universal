﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {

	public GameManager gameManager;

	void Start(){
		gameObject.GetComponent<AudioSource> ().volume = PlayerPreferences.GetGameEffectsVolume ();
	}

	void OnCollisionEnter2D(Collision2D coll) {
		
		if (coll.gameObject.tag == "Destroyable"){
			Destroy (coll.gameObject);
		} 

		if (coll.gameObject.tag == "Box"){
			gameObject.GetComponent<AudioSource> ().Play ();
			gameManager.gameOver ();
		} 

	}

}
