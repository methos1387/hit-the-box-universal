﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeCount : MonoBehaviour {

    public GameObject scoreText;
    Color32 noLife;
    Color32 fullLife;

    private void Start() {
        fullLife = scoreText.GetComponent<Text>().color;
        noLife = fullLife;
        noLife.a = 100;

        InvokeRepeating("FillLife", 0.1f, 1f);
    }

	public void FillLife() {
		for(int i = 0; i < 2; i++){
			this.transform.GetChild (i).GetComponent<SpriteRenderer> ().color = noLife;
		}
		for (int i = 0; i < PlayerPreferences.GetLife(); i++) {
			this.transform.GetChild (i).GetComponent<SpriteRenderer> ().color = fullLife;
		}
	}
		
}
