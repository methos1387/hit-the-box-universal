﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class MainMenuManager : MonoBehaviour {

	private AudioSource myAudio;
	public GameObject canvas;

    bool soundOn;
    bool musicOn;

    Color32 onColor = new Color32(155, 255, 251, 255);
    Color32 offColor = new Color(44, 44, 44, 255);

    public void Awake() {

		myAudio = gameObject.GetComponent<AudioSource> ();

#if UNITY_ANDROID
        PlayGamesPlatform.Activate();
#endif

    }

    public void Start() {

        //PlayerPreferences.SetGold(40000);
        if (!UEncryptPrefs.HasKey("UsingBox")) {
            PlayerPreferences.SetBoxUsing(0);
        }
        Application.targetFrameRate = 60;
        Social.localUser.Authenticate((bool success) => {
        });
    }

	public void ShowLeaderboard() {
		myAudio.Play ();
		if (!Social.localUser.authenticated) {
			Social.localUser.Authenticate ((bool success) => {
				if (success) {
					Social.ShowLeaderboardUI();
				} else {
					return;
				}
			});
		} else {
			Social.ShowLeaderboardUI();
		}
	}

	public void Options() {
		StartCoroutine (waifForAudioOptions());
	}
		
	public void StartGame() {
		StartCoroutine (waitForAudioStartGame ());
	}

    IEnumerator waifForAudioOptions() {
        myAudio.Play();
        canvas.GetComponent<Animator>().SetTrigger("Load");
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadSceneAsync("htb_scene2", LoadSceneMode.Single);

    }

    IEnumerator waitForAudioStartGame() {
        myAudio.Play();
        canvas.GetComponent<Animator>().SetTrigger("Load");
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadSceneAsync("htb_scene1", LoadSceneMode.Single);
    }


    public void ClearPlayerPrefs() {
        PlayerPreferences.SetBoxUsing(0);
        PlayerPreferences.SetBoxSize(1);
        PlayerPreferences.SetBonusSize(1);
        PlayerPreferences.SetBombSize(1);
        PlayerPreferences.SetGold(1);
        UEncryptPrefs.SetInt("IceBox", 0);
        UEncryptPrefs.SetInt("HarmonyBox", 0);
        UEncryptPrefs.SetInt("HolyBox", 0);
    }

}

