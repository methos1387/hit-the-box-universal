﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldText : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		this.GetComponent<Text> ().text = (GameManager.yourScore/100).ToString ();
	}
}
