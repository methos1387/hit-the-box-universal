﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour {

	private BoxScriptC box;

	// Use this for initialization
	void Start () {
		box = GameObject.FindObjectOfType<BoxScriptC> ();
		gameObject.GetComponent<AudioSource> ().volume = PlayerPreferences.GetGameMusicVolume ();	

	}

	void Update() {

		#if !UNITY_EDITOR
		for (var i = 0; i < Input.touchCount; ++i) {
			if (Input.GetTouch(i).phase == TouchPhase.Began) {
				Camera camera = GetComponent<Camera> ();
				RaycastHit2D hitInfo = Physics2D.Raycast(camera.ScreenToWorldPoint(Input.GetTouch(i).position), Vector2.zero);
				// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
				if(hitInfo){
					if (hitInfo.transform.gameObject.name == "TheBox") {
						// Here you can check hitInfo to see which collider has been hit, and act appropriately.
						box.BoxTouched ();
					} else if (hitInfo.transform.gameObject.tag == "Destroyable") {
						hitInfo.transform.gameObject.GetComponent<BonusScript> ().BonusTouched ();
					} else  if (hitInfo.transform.gameObject.tag == "BonusStarter") {
						hitInfo.transform.gameObject.GetComponent<BonusStarterScript>().BonusStarter();
					} else {
						return;
					}
				}
			}
		}
		#endif

		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown (0)) {
			Camera camera = GetComponent<Camera> ();
			Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			RaycastHit2D hitInfo = Physics2D.Raycast(camera.ScreenToWorldPoint(pos), Vector2.zero);
			// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
			if(hitInfo)
			{
				if (hitInfo.transform.gameObject.name == "TheBox") {
					// Here you can check hitInfo to see which collider has been hit, and act appropriately.
					box.BoxTouched ();
				} else if (hitInfo.transform.gameObject.tag == "Destroyable") {
					hitInfo.transform.gameObject.GetComponent<BonusScript> ().BonusTouched ();
				} else if (hitInfo.transform.gameObject.tag == "BonusStarter") {
					hitInfo.transform.gameObject.GetComponent<BonusStarterScript>().BonusStarter();
				} else {
					return;
				}
			}
		}

		#endif
	}	
}
