﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateGold : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("UpdateGoldText", 1, 1);

    }
	
    public void UpdateGoldText() {
        GetComponent<Text>().text = PlayerPreferences.GetGold().ToString();
    }
}
