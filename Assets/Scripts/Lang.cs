﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lang : MonoBehaviour {

	public static string GetLang(string value) {
		string language = Application.systemLanguage.ToString();
		//language = "German";
		if (language == "English") {
			return SearchText (englishText, value);
		} else if (language == "German") {
			return SearchText (germanText, value);
		} else if (language == "ChineseSimplified") {
			return SearchText (chineseSimpleText, value);
		} else if (language == "French") {
			return SearchText (frenchText, value);
		} else if (language == "Spanish") {
			return SearchText (spanishText, value);
		} else if (language == "Hungarian") {
			return SearchText (hungarianText, value);
		} else {
			return SearchText (englishText, value);
		}
	}

	static string SearchText(string[][] textArray, string value) {
		string returnValue = "No language";
		foreach (string[] text in textArray) {
			if (value == text [0]) {
				returnValue = text [1];
				return returnValue;
			} else {
				returnValue = "Text Not found!";
			}
		} 
		return returnValue;
	}

    static string[][] englishText = new string[43][]
    {
        new string[2] {"QuickGame", "Quick Game"},
        new string[2] {"CustomGame" , "Custom Game"},
        new string[2] {"MultiHint", "Enter game name..."},
        new string[2] {"CreateGame", "Create Game"},
        new string[2] {"JoinGame", "Join Game"},
        new string[2] {"ConnectionFailed", "Cannot connect to game server ..."},
        new string[2] {"VideoFailed", "Video ad can't be loaded."},
        new string[2] {"Options", "Options"},
        new string[2] {"Upgrade", "Upgrades"},
        new string[2] {"CurrentGold", "You have:"},
        new string[2] {"Difficulty", "Difficulty"},
        new string[2] {"Easy", "Easy"},
        new string[2] {"Normal", "Normal"},
        new string[2] {"Hard", "Hard"},
        new string[2] {"GameMode", "Game mode"},
        new string[2] {"Default", "Default"},
        new string[2] {"Back", "Back"},
        new string[2] {"Buy", "Buy"},
        new string[2] {"Bought" , "Already bought!"},
        new string[2] {"Max", "Maxed out!"},
        new string[2] {"WaitingForPlayer", "Waiting for a player..."},
        new string[2] {"NoPlayer", "No player found..."},
        new string[2] {"YouWin", "You win!"},
        new string[2] {"YouLose", "You lose!"},
        new string[2] {"Draw", "Draw"},
        new string[2] {"DoubleIt", "Watch Ad to double it..."},
        new string[2] {"SaveIt", "Watch Ad to save it..."},
        new string[2] {"GoldEarned", "Gold earned: "},
        new string[2] {"NoThx", "No, thanks"},
        new string[2] {"YouScore", "Your Score: "},
        new string[2] {"Rate", "If you enjoy the game, would you take a moment, to rate it? Thanks for your support!"},
        new string[2] {"YesRate", "Yes, rate game!"},
        new string[2] {"RemindLater", "Remind me later"},
        new string[2] {"AskUpgrade", "You have 5000 gold, do you want to buy some upgrade?"},
        new string[2] {"Yes", "Yes"},
        new string[2] {"No", "No"},
        new string[2] {"Select", "Select"},
        new string[2] {"Using", "Using"},
        new string[2] {"Continue", "Continue?"},
        new string[2] {"WatchAdContinue", "Watch AD to continue"},
        new string[2] {"MakeBigger", "Make it bigger, easier to hit"},
        new string[2] {"MakeSmaller", "Make it smaller, avoiding to hit"},
        new string[2] {"Loading", "Loading" }
    };

	static string[][] germanText = new string[43][]
	{
		new string[2] {"QuickGame", "Schnelles Spiel"},
		new string[2] {"CustomGame" , "Benutzerdefiniertes Spiel"},
		new string[2] {"MultiHint", "Geben Sie den Spielnamen ein..."},
		new string[2] {"CreateGame", "Spiel erstellen"},
		new string[2] {"JoinGame", "Spiel beitreten"},
		new string[2] {"ConnectionFailed", "Keine Verbindung zum Spielserver ... "},
		new string[2] {"VideoFailed", "Videoanzeige kann nicht geladen werden."},
		new string[2] {"Options", "Einstellungen"},
		new string[2] {"Upgrade", "Upgrades"},
		new string[2] {"CurrentGold", "Du hast:"},
		new string[2] {"Difficulty", "Schwierigkeitsgrad"},
		new string[2] {"Easy", "Einfach"},
		new string[2] {"Normal", "Normal"},
		new string[2] {"Hard", "Schwierig"},
		new string[2] {"GameMode", "Spielmodus"},
		new string[2] {"Default", "Standard"},
		new string[2] {"Back", "Zurück"},
		new string[2] {"Buy", "Kaufen"},
		new string[2] {"Bought" , "Schon gekauft!"},
		new string[2] {"Max", "Maxed out!"},
		new string[2] {"WaitingForPlayer", "Warten auf einen Spieler..."},
		new string[2] {"NoPlayer", "Spieler nicht gefunden..."},
		new string[2] {"YouWin", "Du hast gewonnen!"},
		new string[2] {"YouLose", "Du hast verloren!"},
		new string[2] {"Draw", "Gleichstand!"},
		new string[2] {"DoubleIt", "Werbung ansehen, um es zu verdoppeln..."},
		new string[2] {"SaveIt", "Werbung ansehen, um es zu speichern..."},
		new string[2] {"GoldEarned", "Gold verdient: "},
		new string[2] {"NoThx", "Nein, danke"},
		new string[2] {"YouScore", "Dein Ergebnis: "},
		new string[2] {"Rate", "Wenn Sie das Spiel genießen, würden Sie sich einen Moment Zeit nehmen, um es zu bewerten? Danke für deine Unterstützung!"},
		new string[2] {"YesRate", "Ja, Spiel bewerten!"},
		new string[2] {"RemindLater", "Erinnere mich später"},
		new string[2] {"AskUpgrade", "Du hast 5000 Gold, willst du ein Upgrade kaufen?"},
		new string[2] {"Yes", "Ja"},
		new string[2] {"No", "Nein"},
        new string[2] {"Select", "Wählen"},
        new string[2] {"Using", "Verwenden" },
        new string[2] {"Continue", "Fortsetzen?"},
        new string[2] {"WatchAdContinue", "Werbung ansehen, um fortzufahren" },
        new string[2] {"MakeBigger", "Machen Sie es größer, einfacher zu schlagen"},
        new string[2] {"MakeSmaller", "Mache es kleiner, vermeide es, zu treffen" },
        new string[2] {"Loading", "Wird geladen" }

    };

	static string[][] chineseSimpleText = new string[43][] 
	{
		new string[2] {"QuickGame", "快速游戏"} ,
		new string[2] {"CustomGame" , "自定义游戏"} ,
		new string[2] {"MultiHint", "输入名字…"} ,
		new string[2] {"CreateGame", "创建"} ,
		new string[2] {"JoinGame", "加入"} ,
		new string[2] {"ConnectionFailed", "无法连接到服务器…"} ,
		new string[2] {"VideoFailed", "视频广告无法加载"} ,
		new string[2] {"Options", "设置"} ,
		new string[2] {"Upgrade", "升级"} , 
		new string[2] {"CurrentGold", "你:"} ,
		new string[2] {"Difficulty", "困难"} ,
		new string[2] {"Easy", "简单"} ,
		new string[2] {"Normal", "正常"} ,
		new string[2] {"Hard", "硬"} ,
		new string[2] {"GameMode", "游戏模式"} ,
		new string[2] {"Default", "默认"} ,
		new string[2] {"Back", "背部"} ,
		new string[2] {"Buy", "购买"} ,
		new string[2] {"Bought" , "已经购买!"} ,
		new string[2] {"Max", "棚!"},
		new string[2] {"WaitingForPlayer", "等待一个球员..."},
		new string[2] {"NoPlayer", "玩家没有找到..."},
		new string[2] {"YouWin", "你赢了!"},
		new string[2] {"YouLose", "你输了!"},
		new string[2] {"Draw", "等于"},
		new string[2] {"DoubleIt", "观看广告，使其倍增..."},
		new string[2] {"SaveIt", "观看广告，保存..."},
		new string[2] {"GoldEarned", "赚了金: "},
		new string[2] {"NoThx", "不用了，谢谢"},
		new string[2] {"YouScore", "你的结果: "},
		new string[2] {"Rate", "如果你喜欢这个游戏，你会花点时间来评价一下吗？ 感谢您的支持"},
		new string[2] {"YesRate", "是的，率游戏!"},
		new string[2] {"RemindLater", "稍后提醒我"},
		new string[2] {"AskUpgrade", "你有5000金，你想买一些升级吗？"},
		new string[2] {"Yes", "是"},
		new string[2] {"No", "没有"},
        new string[2] {"Select", "选择"},
        new string[2] {"Using", "运用" },
        new string[2] {"Continue", "继续"},
        new string[2] {"WatchAdContinue", "观看广告以继续" },
        new string[2] {"MakeBigger", "让它更大，更容易击中"},
        new string[2] {"MakeSmaller", "使它变小，避免撞击" },
        new string[2] {"Loading", "装载" }
    };

	
	static string[][] frenchText = new string[43][]
	{
		new string[2] {"QuickGame", "Jeu rapide"} ,
		new string[2] {"CustomGame" , "Jeu personnalisé"} ,
		new string[2] {"MultiHint", "Entrez le nom…"} ,
		new string[2] {"CreateGame", "Créer un jeu"} ,
		new string[2] {"JoinGame", "Rejoins une partie"} ,
		new string[2] {"ConnectionFailed", "Ne peut pas se connecter au serveur... "} ,
		new string[2] {"VideoFailed", "Impossible de charger la publicité vidéo"} ,
		new string[2] {"Options", "Paramètres"} ,
		new string[2] {"Upgrade", "Surclassement"} ,
		new string[2] {"CurrentGold", "Tu as:"} ,
		new string[2] {"Difficulty", "Difficulté"} ,
		new string[2] {"Easy", "Facile"} ,
		new string[2] {"Normal", "Normal"} ,
		new string[2] {"Hard", "Difficile"} ,
		new string[2] {"GameMode", "Mode de jeu"} ,
		new string[2] {"Default", "Défaut"} ,
		new string[2] {"Back", "Arrière"} ,
		new string[2] {"Buy", "Acheter"} ,
		new string[2] {"Bought" , "Déjà acheté!"} ,
		new string[2] {"Max", "Maximum!"},
		new string[2] {"WaitingForPlayer", "Attendre un joueur..."},
		new string[2] {"NoPlayer", "joueur non trouvé..."},
		new string[2] {"YouWin", "Tu as gagné!"},
		new string[2] {"YouLose", "Tu as perdu!"},
		new string[2] {"Draw", "Égal"},
		new string[2] {"DoubleIt", "Regardez une publicité, doublez-la..."},
		new string[2] {"SaveIt", "Regardez une publicité, pour la sauvegarder..."},
		new string[2] {"GoldEarned", "Or gagné: "},
		new string[2] {"NoThx", "Non merci"},
		new string[2] {"YouScore", "Ton score: "},
		new string[2] {"Rate", "Si vous appréciez le jeu, prendriez-vous un moment pour évaluer? Merci pour votre aide!"},
		new string[2] {"YesRate", "Oui, jeu de taux!"},
		new string[2] {"RemindLater", "Me rappeler plus tard"},
		new string[2] {"AskUpgrade", "Vous avez 5000 or, voulez-vous acheter une mise à niveau?"},
		new string[2] {"Yes", "Oui"},
		new string[2] {"No", "Non"},
        new string[2] {"Select", "Sélectionner"},
        new string[2] {"Using", "En utilisant" },
        new string[2] {"Continue", "Continuer"},
        new string[2] {"WatchAdContinue", "Regarder l'annonce pour continuer" },
        new string[2] {"MakeBigger", "Faites-le plus gros, plus facile à frapper"},
        new string[2] {"MakeSmaller", "Faites-le plus petit, en évitant de frapper"},
        new string[2] {"Loading", "Chargement" }
    };

	static string[][] spanishText = new string[43][]
	{
		new string[2] {"QuickGame", "Juego rápido"} ,
		new string[2] {"CustomGame" , "Juego personalizado"} ,
		new string[2] {"MultiHint", "Ingrese su nombre … "} ,
		new string[2] {"CreateGame", "Crear juego"} ,
		new string[2] {"JoinGame", "Unete al juego"} ,
		new string[2] {"ConnectionFailed", "No es posible conectar con el servidor... "} ,
		new string[2] {"VideoFailed", "No se puede cargar el anuncio de vídeo"} ,
		new string[2] {"Options", "Ajustes"} ,
		new string[2] {"Upgrade", "Mejorar"} ,
		new string[2] {"CurrentGold", "Tienes:"} ,
		new string[2] {"Difficulty", "Dificultad"} ,
		new string[2] {"Easy", "Fácil"} ,
		new string[2] {"Normal", "Normal"} ,
		new string[2] {"Hard", "Difícil"} ,
		new string[2] {"GameMode", "Modo de juego"} ,
		new string[2] {"Default", "Defecto"} ,
		new string[2] {"Back", "Espalda"} ,
		new string[2] {"Buy", "Comprar"} ,
		new string[2] {"Bought" , "Ya comprado!"} ,
		new string[2] {"Max", "Ya comprado!"},
		new string[2] {"WaitingForPlayer", "Esperando a un jugador..."},
		new string[2] {"NoPlayer", "jugador no encontrado..."},
		new string[2] {"YouWin", "Ganaste!"},
		new string[2] {"YouLose", "Perdiste!"},
		new string[2] {"Draw", "Igual"},
		new string[2] {"DoubleIt", "Ver un anuncio, para duplicarlo...."},
		new string[2] {"SaveIt", "Ver un anuncio, para guardarlo..."},
		new string[2] {"GoldEarned", "Oro obtenido: "},
		new string[2] {"NoThx", "No, gracias"},
		new string[2] {"YouScore", "Tu puntuación: "},
		new string[2] {"Rate", "Si te gusta el juego, ¿te tomas un momento, para calificarlo? ¡Gracias por su apoyo!"},
		new string[2] {"YesRate", "Si, califica el juego!"},
		new string[2] {"RemindLater", "Recuérdame más tarde"},
		new string[2] {"AskUpgrade", "Tienes 5000 de oro, ¿quieres comprar alguna actualización?"},
		new string[2] {"Yes", "Sí"},
		new string[2] {"No", "No"},
        new string[2] {"Select", "Seleccionar"},
        new string[2] {"Using", "Utilizando" },
        new string[2] {"Continue", "Continuar"},
        new string[2] {"WatchAdContinue", "Mira el anuncio para continuar" },
        new string[2] {"MakeBigger", "Hazlo más grande, más fácil de saludar"},
        new string[2] {"MakeSmaller", "Hazlo más pequeño, evitando golpear"},
        new string[2] {"Loading", "Cargando" }
    } ;

	static string[][] hungarianText = new string[43][]
	{
		new string[2] {"QuickGame", "Gyors játék"} ,
		new string[2] {"CustomGame" , "Testre szabott játék"} ,
		new string[2] {"MultiHint", "Add meg a játék nevét … "} ,
		new string[2] {"CreateGame", "Játék létrehozása"} ,
		new string[2] {"JoinGame", "Belépés a játékba"} ,
		new string[2] {"ConnectionFailed", "Nem lehet kapcsolódni a szerverhez... "} ,
		new string[2] {"VideoFailed", "Hiba a videohirdetést betöltése során"} ,
		new string[2] {"Options", "Beállítások"} ,
		new string[2] {"Upgrade", "Felminősítés"} ,
		new string[2] {"CurrentGold", "Tienes:"} ,
		new string[2] {"Difficulty", "Nehézségi szint"} ,
		new string[2] {"Easy", "Könnyű"} ,
		new string[2] {"Normal", "Közepes"} ,
		new string[2] {"Hard", "Nehéz"} ,
		new string[2] {"GameMode", "Játék mód"} ,
		new string[2] {"Default", "Alapértelmezett"} ,
		new string[2] {"Back", "Vissza"} ,
		new string[2] {"Buy", "Vásárlás"} ,
		new string[2] {"Bought" , "Már megvett!"} ,
		new string[2] {"Max", "Maximális!"},
		new string[2] {"WaitingForPlayer", "Várakozás egy játékosra..."},
		new string[2] {"NoPlayer", "Játékos nem található..."},
		new string[2] {"YouWin", "Nyertél!"},
		new string[2] {"YouLose", "Vesztettél!"},
		new string[2] {"Draw", "Egyenlő"},
		new string[2] {"DoubleIt", "Nézz meg egy hirdetést, hogy megduplázd...."},
		new string[2] {"SaveIt", "Nézz meg egy hirdetést, hogy megmentsd..."},
		new string[2] {"GoldEarned", "Szerzett arany: "},
		new string[2] {"NoThx", "Nem, köszönöm"},
		new string[2] {"YouScore", "Pontszámod: "},
		new string[2] {"Rate", "Ha élvezed a játékot, egy pillanatra időt szakítanál, hogy értékeld? Köszönet a támogatásodért!"},
		new string[2] {"YesRate", "Igen, játék értékelése!"},
		new string[2] {"RemindLater", "Emlékeztess később"},
		new string[2] {"AskUpgrade", "Van 5000 aranyad, szeretnél fejlesztéseket vásárolni?"},
		new string[2] {"Yes", "Igen"},
		new string[2] {"No", "Nem"},
        new string[2] {"Select", "Választ"},
        new string[2] {"Using", "Kiválasztva" },
        new string[2] {"Continue", "Folytatás"},
        new string[2] {"WatchAdContinue", "Nézd meg a hirdetést a folytatáshoz" },
        new string[2] {"MakeBigger", "Legyen nagyobb, könnyebb elkapni"},
        new string[2] {"MakeSmaller", "Legyen kisebb, nehezebb elkapni" },
        new string[2] {"Loading", "Betöltés" }
    } ;

}
