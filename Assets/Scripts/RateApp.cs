﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RateApp : MonoBehaviour {

	public void RateThisApp() {
		PlayerPreferences.SetRate (0);
		#if UNITY_ANDROID
		Application.OpenURL("https://play.google.com/store/apps/details?id=com.schvegler.hitthebox");
		#elif UNITY_IPHONE
		Application.OpenURL("https://itunes.apple.com/de/app/hit-the-box/id1209115164?l=en&mt=8");
		#endif
	}

	public void loadUpgradeScene() {
		SceneManager.LoadScene ("htb_scene2", LoadSceneMode.Single);
	}

	public void noRate() {
		PlayerPreferences.SetRate (0);
	}

}
