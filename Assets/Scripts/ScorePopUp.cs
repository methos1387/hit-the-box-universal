﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScorePopUp : MonoBehaviour {
	private GameObject myCanvas;
	public GameObject ScoreText;

	void startPopUp() {
		StartCoroutine (PopUp ());
	}

	IEnumerator PopUp() {
		//Waits 10 frames , play with this
		for(int i = 0; i < 25; i++) {
			ScoreText.GetComponent<Text> ().fontSize -= 2;
			yield return new WaitForSeconds(0.1f);
		}
	
	}


void Start() {
		if (GameManager.yourScore >= 9000f) {
			ScoreText.GetComponent<Text> ().color = new Color32 (255, 255, 255, 255);
		}
		myCanvas = GameObject.Find ("MainCanvas");
		gameObject.transform.SetParent (myCanvas.transform);
		this.transform.localScale = new Vector2 (1, 1);
		startPopUp ();
	}
	// Update is called once per frame
	void Update () {
		float speedDown = Time.deltaTime * 2;
		this.transform.Translate (Vector3.up * speedDown);
	}
}
