﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RewardScript : MonoBehaviour {

	public GameObject lifeReward;
	public GameObject goldReward;

	// Use this for initialization
	void Start () {
		//lifeReward.GetComponent<Animator> ().SetBool ("LifeIn", true);
		goldReward.GetComponent<Animator> ().SetBool ("GoldIn", true);
	}

}
