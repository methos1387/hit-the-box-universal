﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetTextScript : MonoBehaviour {

	public string value;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Text> ().text = Lang.GetLang (value);
		
	}
}
