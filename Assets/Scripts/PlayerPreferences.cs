﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPreferences : MonoBehaviour {

    public static void SetLife(int value) {
        UEncryptPrefs.SetInt("life", value);
    }
    
    public static int GetLife() {
        if (UEncryptPrefs.HasKey("life")) {
            return UEncryptPrefs.GetInt("life");
        } else {
            PlayerPreferences.SetLife(1);
            return 1;
        }
    }

	public static void SetGemActive(int value) {
        UEncryptPrefs.SetInt ("gem", value);
	}

	public static int GetGem() {
		return UEncryptPrefs.GetInt ("gem");
	}

	public static void SetGold(int value) {
        UEncryptPrefs.SetInt ("gold", value);
	}

	public static int GetGold() {
		return UEncryptPrefs.GetInt ("gold");
	}

	public static void SpendGold(int value) {
		PlayerPreferences.SetGold (PlayerPreferences.GetGold () - value);
	}

	public static void SetBoxSize(int value) {
        UEncryptPrefs.SetInt ("boxSize", value);
	}

	public static int GetBoxSize() {
		return UEncryptPrefs.GetInt ("boxSize");
	}

	public static void SetBonusSize(float value) {
        UEncryptPrefs.SetFloat ("bonusSize", value);
	}

	public static float GetBonusSize() {
		return UEncryptPrefs.GetFloat ("bonusSize");
	}

	public static void SetBombSize(float value) {
        UEncryptPrefs.SetFloat ("bombSize", value);
	}

	public static float GetBombSize() {
		return UEncryptPrefs.GetFloat ("bombSize");
	}

	public static void SetGameMusicVolume(int value) {
        UEncryptPrefs.SetInt ("Music Volume", value);
	}

	public static int GetGameMusicVolume() {
		return UEncryptPrefs.GetInt ("Music Volume");
	}

	public static void SetGameEffectsVolume(int value) {
        UEncryptPrefs.SetInt ("Effects Volume", value);
	}

	public static int GetGameEffectsVolume() {
		return UEncryptPrefs.GetInt ("Effects Volume");
	}

	public static void SetRate(int value) {
        UEncryptPrefs.SetInt ("Rate", value);
	}

	public static int GetRate() {
		if (UEncryptPrefs.HasKey ("Rate")) {
			return UEncryptPrefs.GetInt ("Rate");
		} else {
			return 1;
		}
	}

    public static void BuyIceBox() {
        UEncryptPrefs.SetInt("IceBox", 1);
    }

    public static int IceBoxBought() {
        if (UEncryptPrefs.HasKey("IceBox")) {
            return UEncryptPrefs.GetInt("IceBox");
        } else {
            return 0;
        }
    }

    public static void BuyHarmonyBox() {
        UEncryptPrefs.SetInt("HarmonyBox", 1);
    }

    public static int HarmonyBoxBought() {
        if (UEncryptPrefs.HasKey("HarmonyBox")) {
            return UEncryptPrefs.GetInt("HarmonyBox");
        } else {
            return 0;
        }
    }

    public static void BuyHolyBox() {
        UEncryptPrefs.SetInt("HolyBox", 1);
    }

    public static int HolyBoxBought() {
        if (UEncryptPrefs.HasKey("HolyBox")) {
            return UEncryptPrefs.GetInt("HolyBox");
        } else {
            return 0;
        }
    }

    public static void SetBoxUsing(int box) {
        UEncryptPrefs.SetInt("UsingBox", box);
    }

    public static int GetBoxUsing() {
        if (UEncryptPrefs.HasKey("UsingBox")) {
            return UEncryptPrefs.GetInt("UsingBox");
        } else {
            return 0;
        }
    } 
}
