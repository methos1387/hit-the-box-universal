﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroyScript : MonoBehaviour
{
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SelfDestroy());
    }

   IEnumerator SelfDestroy() {
        yield return new WaitForSeconds(timer);
        Destroy(gameObject);
    }
}
