﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScript : MonoBehaviour {

    float currentPosition;
    float randomSpeed;
    float randomLayer;
    SpriteRenderer spriteRenderer;


	// Use this for initialization
	void Start () {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        currentPosition = gameObject.GetComponent<Transform>().position.x;
        randomSpeed = Random.Range(1f, 3f);
        randomLayer = Random.value;
        if (randomLayer > 0.5f){
            spriteRenderer.sortingLayerName = "Back";
        } 

	}
	

	// Update is called once per frame
	void Update () {
        if (currentPosition < 0) {
            gameObject.transform.Translate(Vector2.right * Time.deltaTime/randomSpeed);
        } else {
            gameObject.transform.Translate(Vector2.left * Time.deltaTime/randomSpeed);
        }

        if (gameObject.transform.position.x > 20f || gameObject.transform.position.x < -20f){
            Destroy(gameObject);
        }

	}
}
