﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExampleUEncryptPrefs : MonoBehaviour 
{

	public Text NameText;
	public Text HitPointsText;
	public Text DamageText;
	public Text MagicText;


	void Start () 
	{
		// Save the prefabs encrypted
		UEncryptPrefs.SetString("PlayerName", "Meldiriel");
		UEncryptPrefs.SetInt("HitPoints",150);
		UEncryptPrefs.SetFloat("Damage",10.05f);
		PlayerPrefs.Save();
		


		// and get them back
		string v_playerName = UEncryptPrefs.GetString("PlayerName");
		int v_hitPoints = UEncryptPrefs.GetInt("HitPoints");
		float v_Damage = UEncryptPrefs.GetFloat("Damage");


		// Assign and save a default value if the key doesn't exists
		float v_Magic = UEncryptPrefs.GetFloat("Magic",25.25f);


		// Display the text on the scene
		if (NameText) NameText.text = "Name: "+v_playerName;
		if (HitPointsText) HitPointsText.text = "Hit Points: "+v_hitPoints.ToString();
		if (DamageText) DamageText.text = "Damage: "+v_Damage.ToString();
		if (MagicText) MagicText.text = "Magic: "+v_Magic.ToString();
	}
}